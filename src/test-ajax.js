import { LitElement, html, css } from 'lit-element';

class TestAjax  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      planet: {type: Object}
    };
  }

  constructor() {
    super();
    this.planet = {};
    this.cargarPlaneta();
  }

  render() {
    return html`
      <p>${this.planet.name}</p>
    `;
  }
  cargarPlaneta() {
    $.get("https://swapi.dev/api/planets/1/", 
    (data, status) => {
      if (status === "success") {
        this.planet = data;
      } else {
        alert("Error llamando rest");
      }
    });
  }
}

customElements.define('test-ajax', TestAjax);