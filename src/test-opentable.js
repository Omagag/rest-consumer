import { LitElement, html, css } from 'lit-element';

class TestOpentable  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        resultados: {type: Object},
        campos: {type: Array},
        fila: {type: Array},
        sig: {type: Boolean},
        prev: {type: Boolean},
        entidades: {type: Array}
    };
  }

  constructor() {
    super();
    this.campos = [];
    this.resultados = { results: [] };
    this.filas = [];
    this.cargarDatos("https://swapi.dev/api/planets/");
    this.entidades = ["planets", "films", "people", "species", "starships", "vehicles"];
  }

  render() {
    return html`
      <div>
        <select id="sel" @change="${this.changeSel}">
          ${this.entidades.map(e => html`<option>${e}</option>`)}
        </select>
      </div>

      <table width="100%">
        <tr>
          ${this.campos.map(campo => html `<th>${campo}</th>`)}
        </tr>
        <!-- ${this.filas.map(i => html `
          <tr>
            ${this.resultados.results[i].valores.map(valor => html `
              <td>${valor}</td>
            `)}
          </tr>
        `)} -->

        <!-- Versión reducida para pintar campos -->
        ${this.resultados.results.map(planet => html `
          <tr>
            ${this.campos.map(campo => html`
            <td>${planet[campo.toString()]}</td>
            `)}
          </tr>
        `)}
      </table>

      ${this.prev?
        html `<div><button @click="${this.anterior}">Anterior</button></div>`: html ``}
      ${this.sig?
        html `<div><button @click="${this.siguiente}">Siguiente</button></div>`: html ``}
    `;
  }
  changeSel() {
    var entidad = this.shadowRoot.querySelector("#sel").value;
    var url = "https://swapi.dev/api/" + entidad + "/";
    this.cargarDatos(url);
  }
  anterior() {
    this.cargarDatos(this.resultados.previous);
  }
  siguiente() {
    this.cargarDatos(this.resultados.next);
  }
  cargarDatos(url) {
    fetch(url)
    .then(response => {
        console.log(response);
        if (!response.ok) { throw response; } // Propaga el response
        return response.json();
    })
    .then(data => {
        console.log(data);
        this.resultados = data;
        this.montarResultados();
    })
    .catch(error => {
        alert("Problemas con el fetch: " + error);
    });
  }
  montarResultados() {
    this.campos = this.getObjProps(this.resultados.results[0]);
    // Este código si funciona; pero es más óptimo trabajarlo desde el render
    // this.filas = [];
    // for (var i = 0; i < this.resultados.results.length; i++) {
    //     this.resultados.results[i].valores = this.getValores(this.resultados.results[i]);
    //     this.filas.push(i);
    // }
    // console.log(this.resultados.results[0][this.campos[0].toString()]);

    this.sig = (this.resultados.next !== null);
    this.prev = (this.resultados.previous !== null);
  }
  // Este código si funciona; pero es más óptimo trabajarlo desde el render
  // getValores(fila) { // obtiene los valores del objeto que se envía (Planet en ese caso)
  //   var valores = [];
  //   for (var i = 0; i < this.campos.length; i++) {
  //       valores[i] = fila[this.campos[i]];
  //   }
  //   return valores;
  // }
  getObjProps(obj) { // Obtiene las propiedades del objeto que se envía (Planet en este caso)
    var props = [];
    for (var prop in obj) {
        if (!this.isArray(this.resultados.results[0][prop])) {
            if (prop !== "created" && prop !== "edited" && prop !== "url") {
                props.push(prop);
            } 
        }
    }
    return props;
  }
  isArray(prop) {
    return Object.prototype.toString.call(prop) === "[object Array]";
  }
}

customElements.define('test-opentable', TestOpentable);