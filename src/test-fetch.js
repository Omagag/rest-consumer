import { LitElement, html, css } from 'lit-element';

class TestFetch  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planets: {type: Object}
    };
  }

  constructor() {
    super();
    this.planets = { results: []}
  }

  render() {
    return html`
      ${this.planets.results.map((planet) => {
            return html`<div>${planet.name} ${planet.rotation_period}</div>`
          })}
    `;
  }
  connectedCallback() {// Sucede cuando termina la carga del componente en su contenedor donde se incluyo; en este caso index.html
    super.connectedCallback();
    try {
        this.cargarPlanetas();
    } catch (e) {
        alert(e);
    }
  }
  cargarPlanetas() {
      fetch("https://swapi.dev/api/planets/") // page as queryParan
        .then(response => {
            console.log(response);
            if (!response.ok) { throw response; } // Propaga el response
            return response.json();
        })
        .then(data => {
            console.log(data);
            this.planets = data;
        })
        .catch(error => {
            alert("Problemas con el fetch: " + error);
        });
  }
}

customElements.define('test-fetch', TestFetch);