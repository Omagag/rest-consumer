import { LitElement, html, css } from 'lit-element';

class BooksList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        data: {type: Object}
    };
  }

  constructor() {
    super();
    this.data = {books: []};
    this.cargarDatos();
  }

  render() {
    return html`
        <div>
            ${this.data.books.map(b => html`${b.titulo} - ${b.autor}`)}
        </div>
    `;
  }
  cargarDatos() {
    fetch("http://localhost:3392/books") // page as queryParan
      .then(response => {
          console.log(response);
          if (!response.ok) { throw response; } // Propaga el response
          return response.json();
      })
      .then(data => {
          console.log(data);
          this.data = data;
      })
      .catch(error => {
          alert("Problemas con el fetch: " + error);
      });
  }
}

customElements.define('books-list', BooksList);