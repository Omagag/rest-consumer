import { LitElement, html, css } from 'lit-element';

class TestXmlhttp  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: {type: Object}
    };
  }

  constructor() {
    super();
    this.cargarPlaneta();
  }

  render() {
    return html`
      <p><code>${this.planet}</code></p>
    `;
  }
  cargarPlaneta() {
      // xmlHttpRequest
      var req = new XMLHttpRequest();
      req.open("GET", "https://swapi.dev/api/planets/1/", true);
      req.onreadystatechange = (aEvt) => { // no se recomienda con function, mejor arrowFunction
        if (req.readyState === 4) { // Indica que el último paquete ha sido recibido
            if (req.status === 200) {
                this.planet = req.responseText;
                this.requestUpdate();
            } else {
                alert("Error llamado rest");
            }
        }
      };
      req.send(null); // null es el body, como es un GET no aplica
  }
}

customElements.define('test-xmlhttp', TestXmlhttp);