import { LitElement, html, css } from 'lit-element';

class BookForm  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        id: {type: Number},
        titulo: {type: String},
        autor: {type: String}
    };
  }

  constructor() {
    super();
    this.id = 0;
    this.titulo = "";
    this.autor = "";
  }

  render() {
    return html`
      <div>
          <label for="id">Id</label>
          <input type="number" id="id" .value="${this.id}" @input="${this.updateId}" />
          <br/>
          <label for="titulo">Título</label>
          <input type="text" id="titulo" .value="${this.titulo}" @input="${this.updateTitulo}" />
          <br/>
          <label for="autor">Autor</label>
          <input type="text" id="autor" .value="${this.autor}" @input="${this.updateAutor}" />
          <br/>

          <button @click="${this.buscarBook}">Buscar</button>
          <button @click="${this.crearBook}">Crear</button>
          <button @click="${this.modificarBook}">Modificar</button>
          <button @click="${this.eliminarBook}">Eliminar</button>
      </div>
    `;
  }
  getCurrentBook() {
      var book = {};
      book.id = this.id;
      book.titulo = this.titulo;
      book.autor = this.autor;
      return book;
  }
  crearBook() {
      var book = this.getCurrentBook();
      const options = {
          method: "POST",
          body: JSON.stringify(book),
          headers: {"Content-Type": "application/json"}
      }
      fetch("http://localhost:3392/books/", options) // page as queryParan
      .then(response => {
          console.log(response);
          if (!response.ok) { throw response; } // Propaga el response
          return response.json();
      })
      .then(data => {
          alert("Book creado");
      })
      .catch(error => {
          alert("Problemas con el fetch: " + error);
      });
  }
  eliminarBook() {
    const options = {
        method: "DELETE",
        headers: {"Content-Type": "application/json"}
    }
    fetch("http://localhost:3392/books/" + this.id, options) // page as queryParan
    .then(response => {
        console.log(response);
        if (!response.ok) { throw response; } // Propaga el response
        return response.json();
    })
    .then(data => {
        alert("Book eliminado");
    })
    .catch(error => {
        alert("Problemas con el fetch: " + error);
    });
  }
  modificarBook() {
    var book = this.getCurrentBook();
    const options = {
        method: "PUT",
        body: JSON.stringify(book),
        headers: {"Content-Type": "application/json"}
    }
    fetch("http://localhost:3392/books/", options) // page as queryParan
    .then(response => {
        console.log(response);
        if (!response.ok) { throw response; } // Propaga el response
        return response.json();
    })
    .then(data => {
        alert("Book modificado");
    })
    .catch(error => {
        alert("Problemas con el fetch: " + error);
    });
  }
  buscarBook() {
    fetch("http://localhost:3392/books/" + this.id) // page as queryParan
      .then(response => {
          console.log(response);
          if (!response.ok) { throw response; } // Propaga el response
          return response.json();
      })
      .then(data => {
          console.log(data);
          this.id = data.id;
          this.titulo = data.titulo;
          this.autor = data.autor;
      })
      .catch(error => {
          alert("Problemas con el fetch: " + error);
      });
  }
  updateId(e) {
      this.id = parseInt(e.target.value);
  }
  updateTitulo(e) {
    this.titulo = e.target.value;
  }
  updateAutor(e) {
    this.autor = e.target.value;
  }
}

customElements.define('book-form', BookForm);