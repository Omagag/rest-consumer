const {When, Then, After} = require('@cucumber/cucumber');
const assert = require('assert');
const {Builder, By, until, Capabilities} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');
const { gunzip } = require('zlib');

When('we request the products list', async function() {
    chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());
    
    // extra configuration
    let options = new chrome.Options();

    this.driver = new Builder()
        //.withCapabilities(Capabilities.chrome()) // extra configuration
        //.setChromeOptions(options) // extra configuration
        .forBrowser("chrome")
        .build();

    this.driver.wait(until.elementLocated(By.className('product-list')));
    await this.driver.get('http://localhost:8000');
});
Then('we should receive', async function(dataTable) { // dataTable son las espectativas de valores que se reciben de Feature
    var productElements = await this.driver.findElements(By.className('producto'));
    var expectations = dataTable.hashes();

    console.log(expectations);

    for (var i = 0; i < expectations.length; i++) {
        // const productName = await productElements[i].findElement(By.tagName('h3')).getText();
        const productName = await productElements[i].findElement(By.css('h3')).getText();
        assert.strictEqual(productName, expectations[i].nombre);

        // const productDesc = await productElements[i].findElement(By.tagName('p')).getText();
        const productDesc = await productElements[i].findElement(By.css('p')).getText();
        assert.strictEqual(productDesc, expectations[i].descripcion);
    }
});
After(async function() {
    this.driver.close();
});