Feature: Lista productos
    Scenario: Cargar lista de productos
        When we request the products list
        Then we should receive
            | nombre | descripcion |
            | Móvil XL | Un teléfono grande con una de las mejores pantallas. |
            | Móvil Mini | Un teléfono mediano con una de las mejores cámaras. |
            | Móvil Standard | Un teléfono estándar. Nada especial. |
        